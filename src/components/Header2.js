
import React, { Component } from 'react';
import './css/home.css';

export default class Header2 extends Component {

state={searchVal:""};

searchText=(ref)=>{
this.setState({searchVal:ref.value});
  
}
render(){
  return (
    <div class="col-lg">
      <nav className={!this.props.bgColor? "navbar navbar-expand-sm bg-light navbar-light":this.props.bgColor}  style={{padding:"3px",shadow: "0px 3px 6px #00000029" ,marginLeft:this.props.bgColor && "-13px",marginRight:this.props.bgColor && "-13px"}} >
   {this.props.bgColor?
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Personal</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Business</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Coperate</a>
          </li>
        
        </ul>
   :
   <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Accounts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Pay & Transfer</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Investment</a>
          </li>
             </ul>
    }
   {this.props.bgColor &&
    <form class="form-inline my-2 my-lg-0">
        <div class="row" style={{display:"block" ,paddingLeft:"160px"}}>
     <input class="searchTxt" type="search" value={this.state.searchVal}  placeholder="Search by keywords" onChange={this.searchText} id="example-search-input"/>
   

     <a  class="headerLinks" href="#">ATM/Branch</a>
     <a  class="headerLinks" href="#">Customer Service</a>
     <a  class="headerLinks" href="#">ATM/Branch</a>
     <a  class="headerLinks" href="#">Appointment</a>     
   
         
      </div>
    </form>}
      </nav>
    </div>


  )

}
}
