
import React, { Component } from 'react';
import Header1 from './Header1';
import Header2 from './Header2';
import MainContainer from './/MainContainer';

function MainLayout(){

    return(

         <div>
           <div class="row">
                    <Header1 />
                </div>
                <div class="row">
                    <Header2 />
                </div>
                <br />
            <MainContainer />
            </div>
    )
}
export default MainLayout;