import React from 'react';
import { Button, Modal, Form, InputGroup } from 'react-bootstrap'
import './css/logincomponent.css';

const LoginModal = (props) => (
	
	<Modal show={props.show} onHide={props.handleClose} className="modalDiv">

		<Modal.Header closeButton>
			<Modal.Title>Two way Verification</Modal.Title>
		</Modal.Header>

		<Modal.Body>
			<p>A 8 digit tocken no has been sent to your mobile no (XXXXXX987) registered with us. Please use the tocken no to sign in to your account</p>
			
			<p class="ModalP">Did not receive Token no?</p>

			<Form class="FormModal">
				<Form.Group >
					<div class="row">
					<span className="ModalText">Tocken No:</span>
					</div>
					<div class="row">
					<input className="tokenTxt" type="text" placeholder="00000000"/>
					<Button className="resendToken" variant="primary">Resend Token</Button>
					</div>
					
				</Form.Group>

				<Form.Group controlId="formBasicPassword">
						<div class="row">
							<span className="ModalText">Password</span>
					
					</div>
						<div class="row">
						<input className="modalPwd" type="password" placeholder="********"/>
						</div>
						<div class="row">
			<Button className="singUpbtn" style={{background:"#0A283C"}} onClick={props.routeNextPage}>
				Now Sign In
          </Button>
		  </div>
				</Form.Group>
			</Form>
		</Modal.Body>
	
	
	</Modal>

)

export default LoginModal;