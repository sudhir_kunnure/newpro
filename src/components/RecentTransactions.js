import React, { Component } from 'react';
import './css/home.css';
const recentTranc = [
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    },
    {
        head: '$15',
        line1: '01/10/2020',
        line2: 'At Tesco'
    }
]

function RecentTransactions() {

    return (
        <div style={{ background: "#FFFFFF", boxShadow: "0px 6px 12px #00000029", padding: "20px", width: "100%" }}>
            <div className="row">
                <p className="tranc">Recent Transaction(********9893)</p>

            </div>


            <div className="row" >
                {recentTranc.map((tranc,index) => {
                    return <div className="row">

                        <div  className="col-lg transaction">
                            <p className="tranc-head">{tranc.head}</p>
                            <p>{tranc.line1}</p>
                            <p>{tranc.line2}</p>
                        </div>
                       {index!=recentTranc.length-1 && <div class="col-sm rBorder"></div>}
                    </div>
                })}
            </div>
        </div>

    )
}
export default RecentTransactions;
