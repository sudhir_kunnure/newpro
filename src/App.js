import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter
   
} from "react-router-dom";
// import { hashHistory } from "react-router";

import logo from './logo.svg';
import './App.css';
import LoginComponents from './components/LoginComponents';
import MainLayout from './components/MainLayout';


class App extends Component {
  
  render() {
    return (
      <div className="Appbackcolor">
        <Switch>
          <Route path="/" exact>
            <LoginComponents history={this.props.history}/>
          </Route>
          <Route path="/home" component={MainLayout} />
        </Switch>

      </div>
    );
  }
}

export default withRouter(App);
